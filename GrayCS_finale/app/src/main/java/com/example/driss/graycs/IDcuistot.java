package com.example.driss.graycs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class IDcuistot extends AppCompatActivity {

    private EditText mdp;
    private Intent choixRU;
    private String message;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idcuistot);
        mdp = findViewById(R.id.editText);

    }

    public void enregistrement(View view) {
        message = mdp.getText().toString();
        String motdepasse = "cuist0t";
        if (message.equals(motdepasse)) {
            choixRU = new Intent();
            choixRU.setClass(this, ChoixRU.class);
            startActivity(choixRU);
        }
        else {
            Toast.makeText(this,"Mot de passe incorrect",Toast.LENGTH_SHORT).show();

        }
    }
}