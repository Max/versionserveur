package com.example.driss.graycs;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MuseeMainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etEntree;
    EditText etPlat;
    EditText etDessert;
    EditText etAffluence;
    LinearLayout ll;
    Button bplat, bAff, bReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musee_main);
        ll = findViewById(R.id.monLayout);

        enregistrerMenu();

    }

    private void enregistrerMenu() {


        etPlat = new EditText(this);
        etPlat.setHint("Indiquer le plat");
        etPlat.setTag("plat");
        ll.addView(etPlat);
        bplat = new Button(this);
        bplat.setText("Enregistrer");
        bplat.setTag("plat");
        bplat.setOnClickListener(this);
        ll.addView(bplat);



        etAffluence = new EditText(this);
        etAffluence.setHint("Indiquer l'affluence");
        etAffluence.setTag("affluence");
        ll.addView(etAffluence);
        bAff = new Button(this);
        bAff.setText("Enregistrer");
        bAff.setTag("affluence");
        bAff.setOnClickListener(this);
        ll.addView(bAff);

        bReset = new Button(this);
        bReset.setText("Réinitialiser les menus");
        bReset.setTag("reset");



    }

    public void onClick(View view) {
        Button bClique = (Button) view;

        if (bClique.getTag().equals("reset")){
            envoyerPlat("reset", "");

        }
        else {
            String typePlat = bClique.getTag().toString();
            EditText etCuisinier = ll.findViewWithTag(typePlat);
            String platEcrit = etCuisinier.getText().toString();

            envoyerPlat(typePlat, platEcrit);
        }
    }

    private void envoyerPlat(String typePlat, String plat){
        final String typeDePlat = typePlat;
        final String nouveauPlat = plat;

        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "http://10.0.2.2:8084/Menu_RU/webresources/restaurants/Musee";
        Response.Listener responseListener = new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                Log.i("cuistotMusee", "ok http");
                //requete acceptee

            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {//erreur
                Log.i("cuistotMusee", "erreur http");
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("typeDePlat", typeDePlat);
                params.put("nouveauPlat", nouveauPlat);
                return params;
            }
        };
        queue.add(stringRequest);

        Toast.makeText(this, typeDePlat + " enregistré(e)", Toast.LENGTH_SHORT).show();

    }
}