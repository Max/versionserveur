package com.example.driss.graycs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Spinner;

import java.util.Vector;


public class ChoixRU extends AppCompatActivity implements View.OnClickListener {
    EditText etMenu;
    Spinner spinnerRu;
    Button bEnrMenu;
    String menu;
    LinearLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_ru);
        ll = findViewById(R.id.monLayout);
        choixRu();

    }

    private void choixRu() {
        TextView choisirRu = new TextView(this);
        choisirRu.setText("Choisissez le RU");
        ll.addView(choisirRu);

        Button bRuEiffel = new Button(this);
        bRuEiffel.setText("Eiffel");
        bRuEiffel.setOnClickListener(this);
        ll.addView(bRuEiffel);

        Button bRuBreguet = new Button(this);
        bRuBreguet.setText("Breguet");
        bRuBreguet.setOnClickListener(this);
        ll.addView(bRuBreguet);

        Button bMusee = new Button(this);
        bMusee.setText("Musee");
        bMusee.setOnClickListener(this);
        ll.addView(bMusee);

    }

    public void onClick(View view) {
        Button b = (Button) view;
        String resto = b.getText().toString();
        Intent VersRU = new Intent();
        if (resto.equals("Breguet")){
            VersRU.setClass(this, BreguetMainActivity.class);
        }
        if (resto.equals("Eiffel")){
            VersRU.setClass(this,EiffelMainActivity.class);
        }
        if (resto.equals("Musee")){
            VersRU.setClass(this,MuseeMainActivity.class);
        }

        startActivity(VersRU);

        }
}

