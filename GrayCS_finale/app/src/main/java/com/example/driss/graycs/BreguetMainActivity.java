package com.example.driss.graycs;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BreguetMainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etEntree;
    EditText etPlat;
    EditText etDessert;
    EditText etAffluence;
    LinearLayout ll;
    JSONObject menu = new JSONObject();
    String entree, plat, dessert, affluence;
    Button bEnr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breguet_main);
        ll = findViewById(R.id.monLayout);

        bEnr = new Button(this);
        bEnr.setText("Enregistrer");
        bEnr.setOnClickListener(this);
        ll.addView(bEnr);

    }




//en appuyant sur le bouton "Enregistrer on envoie un fichier JSON au serveur contenant l'entree, le plat et le dessert.
    public void onClick(View view) {
        //on recupere les textes
        etEntree = findViewById(R.id.editTextEntree);
        etPlat = findViewById(R.id.editTextPlat);
        etDessert = findViewById(R.id.editTextDessert);
        etAffluence = findViewById(R.id.editTextAffluence);
        Log.d("test clique","ok");
        entree = etEntree.getText().toString();
        plat = etPlat.getText().toString();
        dessert = etDessert.getText().toString();
        affluence = etAffluence.getText().toString();

//creation du JSON a envoyer au serveur
        try {
            menu.put("entree", entree);
            menu.put("plat", plat);
            menu.put("dessert", dessert);
            menu.put("affluence", affluence);
            Log.d("renseigner menu","json menu cree");
        } catch(org.json.JSONException e){
            Log.d("renseigner menu","erreur menu");
        }
//creation de la requete
        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "http://10.0.2.2:8084/Menu_RU/webresources/restaurants/Breguet";
        Response.Listener responseListener = new Response.Listener() {
            @Override
            public void onResponse(Object response){
                Log.i("cuistotBreguet", "ok http");
                //requete acceptee

            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {//erreur
                Log.i("cuistotBreguet", "erreur http");
            }
        };
        //requete POST vers le serveur avec le fichier contenant les infos
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("menu", menu.toString());
                return params;
            }
        };
        queue.add(stringRequest);

        Toast.makeText(this, "menu enregistre", Toast.LENGTH_SHORT).show();


    }
}
