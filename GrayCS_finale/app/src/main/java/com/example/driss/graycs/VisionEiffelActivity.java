package com.example.driss.graycs;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class VisionEiffelActivity extends AppCompatActivity {
    String entree = "Rien";
    String plat = "Rien";
    String dessert = "Rien";
    String affluence = "Rien";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vision_eiffel);

        recevoirMenuBreguet();


    }

    private void recevoirMenuBreguet() {


        String url = "http://10.0.2.2:8084/Menu_RU/webresources/restaurants/Eiffel";
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        Response.Listener<String> responseListener = new VisionEiffelActivity.EiffelResponseListener();
        Response.ErrorListener responseErrorListener = new VisionEiffelActivity.EiffelErrorListener();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, responseListener, responseErrorListener);
        requestQueue.add(stringRequest);



    }

    private class EiffelResponseListener implements Response.Listener<String> {

        @Override
        public void onResponse(String response) {

            try {
                JSONObject menu = new JSONObject(response);


                entree = menu.getString("entree");
                plat = menu.getString("plat");
                dessert = menu.getString("dessert");
                affluence = menu.getString("affluence");
                Log.i("getMenuEiffel", "ok http");

                TextView tv = findViewById(R.id.textView2);
                tv.setText("Entrees : "+ entree);

                TextView tv2 = findViewById(R.id.textView6);
                tv2.setText("Plat : "+ plat);

                TextView tv3 = findViewById(R.id.textView7);
                tv3.setText("Desserts : "+ dessert);

                TextView tv4=findViewById(R.id.textView14);
                tv4.setText("Affluence : "+ affluence);

            } catch (Exception e) {
                Log.i("getMenuEiffel","erreur catch");
            }


        }
    }

    private class EiffelErrorListener implements Response.ErrorListener {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.i("getMenuEiffel","erreur http");

        }


    }
    }

