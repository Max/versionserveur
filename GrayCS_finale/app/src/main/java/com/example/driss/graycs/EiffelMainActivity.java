package com.example.driss.graycs;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class EiffelMainActivity extends AppCompatActivity implements View.OnClickListener {
    EditText etEntree;
    EditText etPlat;
    EditText etDessert;
    EditText etAffluence;
    LinearLayout ll;
    Button bentree, bplat, bdessert, bAff;
    String typeDePlat, nouveauPlat, platEcrit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eiffel_main);
        ll = findViewById(R.id.monLayout);

        enregistrerMenu();

    }

    private void enregistrerMenu() {
        etEntree = new EditText(this);
        etEntree.setHint("Indiquer l'entrée");
        etEntree.setTag("entree");
        ll.addView(etEntree);
        bentree = new Button(this);
        bentree.setText("Enregistrer");
        bentree.setTag("entree");
        bentree.setOnClickListener(this);
        ll.addView(bentree);


        etPlat = new EditText(this);
        etPlat.setHint("Indiquer le plat");
        etPlat.setTag("plat");
        ll.addView(etPlat);
        bplat = new Button(this);
        bplat.setText("Enregistrer");
        bplat.setTag("plat");
        bplat.setOnClickListener(this);
        ll.addView(bplat);


        etDessert = new EditText(this);
        etDessert.setHint("Indiquer le dessert");
        etDessert.setTag("dessert");
        ll.addView(etDessert);
        bdessert = new Button(this);
        bdessert.setText("Enregistrer");
        bdessert.setTag("dessert");
        bdessert.setOnClickListener(this);
        ll.addView(bdessert);


        etAffluence = new EditText(this);
        etAffluence.setHint("Indiquer l'affluence");
        etAffluence.setTag("affluence");
        ll.addView(etAffluence);
        bAff = new Button(this);
        bAff.setText("Enregistrer");
        bAff.setTag("affluence");
        bAff.setOnClickListener(this);
        ll.addView(bAff);

    }

    public void onClick(View view) {
        Button bClique = (Button) view;
        typeDePlat = bClique.getTag().toString();

        Button b = new Button(this);
        b.setText(typeDePlat);
        ll.addView(b);

        EditText etCuisinier = ll.findViewWithTag(typeDePlat);
        platEcrit = etCuisinier.getText().toString();

        envoyerPlat();
    }

    private void envoyerPlat(){


        RequestQueue queue = Volley.newRequestQueue(this);

        String url = "http://10.0.2.2:8084/Menu_RU/webresources/restaurants/Eiffel";
        Response.Listener responseListener = new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                Log.i("cuistotEiffel", "ok http");
                //requete acceptee

            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {//erreur
                Log.i("cuistotEiffel", "erreur http");
            }
        };
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, responseListener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("typePlat", typeDePlat);
                params.put("plat", platEcrit);
                return params;
            }
        };
        queue.add(stringRequest);

        Toast.makeText(this, typeDePlat + " enregistré(e)", Toast.LENGTH_SHORT).show();

    }
}