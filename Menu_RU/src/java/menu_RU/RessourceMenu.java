/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu_RU;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * REST Web Service
 *
 * @author Florian_pro
 */
@Path("restaurants")
public class RessourceMenu {

    @Context
    private UriInfo context;
    
    protected static restaurantSimple ruBreguet = new restaurantSimple("Breguet");
    protected static restaurantSimple ruEiffel = new restaurantSimple("Eiffel");
    protected static restaurantSimple Musee = new restaurantSimple("Musee");

    /**
     * Creates a new instance of RessourceMenu
     */
    public RessourceMenu() {
    }

    /**
     * Retrieves representation of an instance of menu_RU.RessourceMenu
     * @return an instance of java.lang.String
     */
    @GET
    @Path("Breguet")
    @Produces(MediaType.TEXT_PLAIN)
    public String getMenuBreguet() {
        //TODO return proper representation object
        return ruBreguet.getMenu().toString();
    }
    @GET
    @Path("Eiffel")
    @Produces(MediaType.TEXT_PLAIN)
    public String getMenuEiffel() {
        //TODO return proper representation object
        return ruEiffel.getMenu().toString();
    }
    
    @GET
    @Path("Musee")
    @Produces(MediaType.TEXT_PLAIN)
    public String getPlatsManquantsMusee() {
        //TODO return proper representation object
        JSONObject retour = new JSONObject();
        retour.put("affluence", Musee.getAfffluence());
        retour.put("platsManquants", Musee.getPlatsManquants());
        return Musee.getPlatsManquants().toString();
    }
    
 

    /**
     * PUT method for updating or creating an instance of RessourceMenu
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    @POST
    @Path("Breguet")
    @Produces(MediaType.TEXT_PLAIN)
    public void modifierMenuBreguet(@FormParam("menu") String nouveauMenu){
        JSONParser jsnParser = new JSONParser();
        try{
            JSONObject menu = (JSONObject) jsnParser.parse(nouveauMenu);
            String entree = menu.get("entrée").toString();
            String plat = menu.get("plat").toString();
            String dessert = menu.get("dessert").toString();
            String affluence = menu.get("affluence").toString();
            ruBreguet.setEntree(entree);
        ruBreguet.setPlat(plat);
        ruBreguet.setDessert(dessert);
        ruBreguet.setAffluence(affluence);
            
          
        }
        catch (org.json.simple.parser.ParseException e){
            e.printStackTrace();
        }
        
        
        
        
        
    }
    @POST
    @Path("Eiffel")
    @Produces(MediaType.TEXT_PLAIN)
    public void modifierMenuEiffel(@FormParam("typePlat") String typePlat, @FormParam("plat") String plat){
        if (typePlat.equals("entree")){
        ruEiffel.setEntree(plat);
   }
    if (typePlat.equals("plat")){
        ruEiffel.setPlat(plat);
   }
    
    if (typePlat.equals("dessert")){
        ruEiffel.setDessert(plat);
   }
    
    if (typePlat.equals("affluence")){
        ruEiffel.setAffluence(plat);
   }
}

    @POST
    @Path("Musee")
    @Produces(MediaType.TEXT_PLAIN)
    public void modifierMenuMusee(@FormParam("typeDePlat") String typePlat, @FormParam("nouveauPlat") String plat){
    
    if (typePlat.equals("affluence")){
        Musee.setAffluence(plat);
   }
    
    if (typePlat.equals("plat")){
        Musee.ajouterPlatManquant(plat);
    }
    //reinitialiser les plats
    if (typePlat.equals("reset")){
        Musee.resetPlatsManquants();
    }
}


}
