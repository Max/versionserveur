/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package menu_RU;
import org.json.simple.JSONObject;
/**
 *
 * @author Florian_pro
 */
//creation de la classe restaurant, un objet restaurant contiendra un nom, une entree, un plat etc
//ainsi que des plats manquants pour le musee
public class restaurantSimple {
    private String entree;
    private String plat;
    private String dessert;
    private String affluence;
    private String platsManquants;
    private String nomRU;
    
    private JSONObject menu;
            
    public restaurantSimple(String nom){
        this.entree = "Pas d'entree renseignee";
        this.plat = "Pas de plat renseigne";
        this.dessert = "Pas de dessert renseigne";
        this.affluence = "Affluence inconnue";
        this.platsManquants = "Tous les plats sont disponibles";
        this.menu = new JSONObject();
        
        
        menu.put("entree",entree);
        menu.put("plat",plat);
        menu.put("dessert",dessert);
        menu.put("affluence",affluence);
        
        this.nomRU = nom;
    }
    
    public void setEntree(String nouvelleEntree){
        this.entree = nouvelleEntree;
        menu.put("entree", plat);
    }
    public void setPlat(String nouveauPlat){
        this.plat = nouveauPlat;
        menu.put("plat", plat);
    }
    public void setDessert(String nouveauDessert){
        this.dessert = nouveauDessert;
        menu.put("dessert", plat);
    }
    
    public void setAffluence(String nouveauDessert){
        this.affluence = nouveauDessert;
        menu.put("affluence", plat);
    }
    
    public void ajouterPlatManquant(String nouveauPlatManquant){
        platsManquants = platsManquants + ", " + nouveauPlatManquant;
    }
    
    
 
    
    public String getPlatsManquants(){
        return this.platsManquants;
    }
    
    public void resetPlatsManquants(){
        platsManquants = "";
    }
    
    
    public String getEntree(){
        return this.entree;
    }
    
    public String getPlat(){
        return this.plat;
    }
    
    public String getDessert(){
        return this.dessert;
    }
    
    public String getAfffluence(){
        return this.affluence;
    }
    
    public JSONObject getMenu(){
        return this.menu;
    }
}
